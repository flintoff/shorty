# frozen_string_literal: true

require 'simple_command'
require_relative './../lib/short_code'

# Validate shortcode for existing. Pass if not exist
class UrlValidateShortcodeExist
  prepend SimpleCommand

  def initialize(short_code)
    @short_code = short_code
  end

  def call
    unless @short_code.to_s.empty?
      if ShortCode.exist?(@short_code)
        errors.add(
          :urlValidateShortcode,
          code: 409,
          message: 'The the desired shortcode is already in use. Shortcodes are case-sensitive.'
        )
        return false
      end
    end

    true
  end
end
