# frozen_string_literal: true

require 'simple_command'
require_relative './../lib/short_code'
require_relative './url_validate_shortcode_format'
require_relative './url_validate_shortcode_not_found'

# Return statistics
class UrlStats
  prepend SimpleCommand

  def initialize(short_code)
    @short_code = short_code
  end

  def call
    out = UrlValidateShortcodeFormat.call(@short_code, true)
    unless out.success?
      errors.add_multiple_errors(out.errors)
      return false
    end

    out = UrlValidateShortcodeNotFound.call(@short_code)
    unless out.success?
      errors.add_multiple_errors(out.errors)
      return false
    end

    redirect_count = ShortCode.get_redirect_count(@short_code)

    out = {
      startDate: ShortCode.iso8601(ShortCode.get_content(@short_code)['startDate']),
      redirectCount: redirect_count
    }

    out[:lastSeenDate] = ShortCode.iso8601(ShortCode.get_last_seen_date(@short_code)) if redirect_count > 0
    out
  end
end
