# frozen_string_literal: true

require 'simple_command'
require_relative './../lib/short_code'

# Validation of shortcode. Pass if shortcode is present
class UrlValidateShortcodeNotFound
  prepend SimpleCommand

  def initialize(shortcode)
    @shortcode = shortcode
  end

  def call
    unless ShortCode.exist?(@shortcode)
      errors.add(
        :urlValidateShortcode,
        code: 404,
        message: 'The shortcode cannot be found in the system'
      )
      return false
    end

    true
  end
end
