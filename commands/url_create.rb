# frozen_string_literal: true

require 'simple_command'
require_relative './../lib/short_code'
require_relative './url_validate_url'
require_relative './url_validate_shortcode_exist'
require_relative './url_validate_shortcode_format'

# Validate and create new record
class UrlCreate
  prepend SimpleCommand

  def initialize(url, short_code)
    @url = url
    @short_code = short_code
  end

  def call
    out = UrlValidateUrl.call(@url)
    unless out.success?
      errors.add_multiple_errors(out.errors)
      return false
    end

    out = UrlValidateShortcodeFormat.call(@short_code, false)
    unless out.success?
      errors.add_multiple_errors(out.errors)
      return false
    end

    out = UrlValidateShortcodeExist.call(@short_code)
    unless out.success?
      errors.add_multiple_errors(out.errors)
      return false
    end

    @short_code = ShortCode.generate if @short_code.to_s.empty?
    @short_code = ShortCode.generate while ShortCode.exist?(@short_code)

    ShortCode.create(@url, @short_code)
    @short_code
  end
end
