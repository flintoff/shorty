# frozen_string_literal: true

require 'simple_command'
require_relative './../lib/short_code'

# Validation of shortcode. Pass if shortcode have correct format
class UrlValidateShortcodeFormat
  prepend SimpleCommand

  def initialize(shortcode, require = false)
    @shortcode = shortcode
    @require = require
  end

  def call
    return true if !@require && @shortcode.to_s.empty?
    unless ShortCode.valid?(@shortcode)
      errors.add(
        :urlValidateShortcode,
        code: 422,
        message: 'The shortcode fails to meet the following regexp: ^[0-9a-zA-Z_]{4,}$.'
      )
      return false
    end
    true
  end
end
