# frozen_string_literal: true

require 'simple_command'
require_relative './../lib/short_code'

# Validation of url. Pass if url is present
class UrlValidateUrl
  prepend SimpleCommand

  def initialize(url)
    @url = url
  end

  def call
    if @url.to_s.empty?
      errors.add(
        :urlValidateUrl,
        code: 400,
        message: 'url is not present'
      )
      return false
    end

    unless valid_url?(@url)
      errors.add(
        :urlValidateUrl,
        code: 422,
        message: 'url is not valid'
      )
      return false
    end
    true
  end

  private

  def valid_url?(url)
    url_regexp = /^(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/ix
    url =~ url_regexp ? true : false
  end
end
