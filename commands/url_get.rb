# frozen_string_literal: true

require 'simple_command'
require_relative './../lib/short_code'
require_relative './url_validate_shortcode_format'
require_relative './url_validate_shortcode_not_found'

# Receive shortcode and return url
class UrlGet
  prepend SimpleCommand

  def initialize(short_code)
    @short_code = short_code
  end

  def call
    out = UrlValidateShortcodeFormat.call(@short_code, true)
    unless out.success?
      errors.add_multiple_errors(out.errors)
      return false
    end

    out = UrlValidateShortcodeNotFound.call(@short_code)
    unless out.success?
      errors.add_multiple_errors(out.errors)
      return false
    end

    ShortCode.increment_stats(@short_code)
    ShortCode.get_content(@short_code)['url']
  end
end
