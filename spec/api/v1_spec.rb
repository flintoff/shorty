# frozen_string_literal: true

require 'rspec-grape'
require 'ostruct'
require_relative './../spec_helper'
require_relative './../../api/api.rb'

describe API, type: :api do
  context 'POST /shorten' do
    it 'when the context is successful' do
      shortcode = 'example'
      os = OpenStruct.new(success?: true, result: shortcode)
      allow(UrlCreate).to receive(:call).and_return(os)
      post '/shorten', ''
      expect(last_response.status).to eq 201
      expect(JSON.parse(last_response.body)['shortcode']).to eq shortcode
    end

    it 'when the context is not successful' do
      error_code = 400
      error_message = 'qwertyQWERTY 1234567890 !@#$%^&*()_+'
      os = OpenStruct.new(
        success?: false,
        errors: { key: [{ code: error_code, message: error_message }] }
      )
      allow(UrlCreate).to receive(:call).and_return(os)
      post '/shorten', ''
      expect(last_response.status).to eq error_code
      expect(last_response.body).to eq error_message
    end
  end

  context 'GET /:shortcode' do
    it 'when the context is successful' do
      url = 'http://example.com'
      allow(UrlGet).to receive(:call).and_return(OpenStruct.new(success?: true, result: url))

      get '/example'
      expect(last_response.status).to eq 302
      expect(last_response.body).to include("Location: #{url}")
    end

    it 'when the context is not successful' do
      error_code = 400
      error_message = 'qwertyQWERTY 1234567890 !@#$%^&*()_+'
      os = OpenStruct.new(
        success?: false,
        errors: {
          key: [{ code: error_code, message: error_message }]
        }
      )
      allow(UrlGet).to receive(:call).and_return(os)
      get '/example'
      expect(last_response.status).to eq error_code
      expect(last_response.body).to eq error_message
    end
  end

  context 'GET /:shortcode/stats' do
    it 'when the context is successful' do
      result = {
        redirectCount: 1,
        startDate: '2012-04-23T18:25:43.511Z',
        lastSeenDate: '2012-04-23T18:25:43.511Z'
      }
      allow(UrlStats).to receive(:call).and_return(OpenStruct.new(success?: true, result: result))

      get '/example/stats'
      expect(last_response.status).to eq 200
      expect(last_response.body).to eq result.to_json
    end

    it 'when the context is not successful' do
      error_code = 400
      error_message = 'qwertyQWERTY 1234567890 !@#$%^&*()_+'
      os = OpenStruct.new(
        success?: false,
        errors: {
          key: [{ code: error_code, message: error_message }]
        }
      )
      allow(UrlStats).to receive(:call).and_return(os)
      get '/example/stats'
      expect(last_response.status).to eq error_code
      expect(last_response.body).to eq error_message
    end
  end
end
