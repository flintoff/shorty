# frozen_string_literal: true

require_relative './../spec_helper'

describe UrlValidateUrl do
  subject(:context) { described_class.call(url) }

  describe '.call' do
    context 'when the context is successful' do
      let(:url) { 'http://example.com' }

      it 'succeeds' do
        expect(context).to be_success
      end
    end

    context 'when url is empty' do
      let(:url) { '' }

      it 'fails' do
        error = context.errors.values.flatten.first
        expect(context).to be_failure
        expect(error[:code]).to eq 400
        expect(error[:message]).to eq 'url is not present'
      end
    end

    context 'when url is not valid' do
      let(:url) { 'httpp://example.com' }

      it 'fails' do
        error = context.errors.values.flatten.first
        expect(context).to be_failure
        expect(error[:code]).to eq 422
        expect(error[:message]).to eq 'url is not valid'
      end
    end
  end
end
