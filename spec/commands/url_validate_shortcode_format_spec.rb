# frozen_string_literal: true

require_relative './../spec_helper'

describe UrlValidateShortcodeFormat do
  subject(:context) { described_class.call(short_code, require) }

  describe '.call' do
    context 'when the context is successful' do
      let(:short_code) { 'TeSt_0123456789' }
      let(:require) { true }

      it 'succeeds' do
        expect(context).to be_success
      end
    end

    context 'when the context is not successful' do
      let(:short_code) { 't0' }
      let(:require) { true }

      it 'fails' do
        error = context.errors.values.flatten.first
        expect(context).to be_failure
        expect(error[:code]).to eq 422
        expect(error[:message]).to eq 'The shortcode fails to meet the following regexp: ^[0-9a-zA-Z_]{4,}$.'
      end
    end
  end
end
