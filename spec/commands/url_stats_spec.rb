# frozen_string_literal: true

require_relative './../spec_helper'

describe UrlStats do
  subject(:context) { described_class.call(short_code) }

  after(:each) do
    ShortCode.flush_all
  end

  describe '.call' do
    context 'with 0 redirects' do
      let(:short_code) { 'example' }

      before do
        ShortCode.create('http://example.com', short_code)
      end
      it 'succeeds' do
        expect(context).to be_success
        expect(context.result[:redirectCount]).to eq 0
        expect(context.result[:startDate]).to match(/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}Z$/)
        expect(context.result.key?(:lastSeenDate)).to be false
      end
    end

    context 'with one redirect' do
      let(:short_code) { 'example' }

      before do
        ShortCode.create('http://example.com', short_code)
        ShortCode.increment_stats(short_code)
      end
      it 'succeeds' do
        expect(context).to be_success
        expect(context.result[:redirectCount]).to eq 1
        expect(context.result[:startDate]).to match(/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}Z$/)
        expect(context.result[:lastSeenDate]).to match(/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}Z$/)
      end
    end

    context 'when shortcode cannot be found in the system' do
      let(:short_code) { 'example' }

      it 'fails' do
        expect(context).to be_failure
      end
    end

    context 'when shortcode is not valid' do
      let(:short_code) { 'e1' }

      it 'fails' do
        expect(context).to be_failure
      end
    end
  end
end
