# frozen_string_literal: true

require_relative './../spec_helper'

describe UrlValidateShortcodeNotFound do
  subject(:context) { described_class.call(short_code) }

  describe '.call' do
    context 'when the context is successful' do
      let(:short_code) { 'test_exist' }

      before { ShortCode.create('test', short_code) }
      it 'succeeds' do
        expect(context).to be_success
      end
      after { ShortCode.delete(short_code) }
    end

    context 'when the context is not successful' do
      let(:short_code) { 'test_not_found' }

      it 'fails' do
        error = context.errors.values.flatten.first
        expect(context).to be_failure
        expect(error[:code]).to eq 404
        expect(error[:message]).to eq 'The shortcode cannot be found in the system'
      end
    end
  end
end
