# frozen_string_literal: true

require_relative './../spec_helper'

describe UrlValidateShortcodeExist do
  subject(:context) { described_class.call(short_code) }

  describe '.call' do
    context 'when the context is successful' do
      let(:short_code) { 'test_not_exist' }

      it 'succeeds' do
        expect(context).to be_success
      end
    end

    context 'when the context is not successful' do
      let(:short_code) { 'test_exist' }

      before { ShortCode.create('test', short_code) }
      it 'fails' do
        error = context.errors.values.flatten.first
        expect(context).to be_failure
        expect(error[:code]).to eq 409
        expect(error[:message]).to eq 'The the desired shortcode is already in use. Shortcodes are case-sensitive.'
      end
      after { ShortCode.delete(short_code) }
    end
  end
end
