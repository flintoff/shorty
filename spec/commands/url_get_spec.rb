# frozen_string_literal: true

require_relative './../spec_helper'

describe UrlGet do
  subject(:context) { described_class.call(short_code) }

  after(:each) do
    ShortCode.flush_all
  end

  describe '.call' do
    context 'when the context is successful' do
      let(:short_code) { 'example' }

      before { ShortCode.create('http://example.com', short_code) }
      it 'succeeds' do
        expect(context).to be_success
        expect(context.result).to eq 'http://example.com'
      end
    end

    context 'when shortcode cannot be found in the system' do
      let(:short_code) { 'example' }

      it 'fails' do
        expect(context).to be_failure
      end
    end

    context 'when shortcode is not valid' do
      let(:short_code) { 'e1' }

      it 'fails' do
        expect(context).to be_failure
      end
    end
  end
end
