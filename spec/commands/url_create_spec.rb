# frozen_string_literal: true

require_relative './../spec_helper'

describe UrlCreate do
  subject(:context) { described_class.call(url, short_code) }

  after(:each) do
    ShortCode.flush_all
  end

  describe '.call' do
    context 'when transmit param shortcode' do
      let(:url) { 'http://example.com' }
      let(:short_code) { 'example' }

      it 'succeeds' do
        expect(context).to be_success
        expect(context.result).to eq short_code
      end
    end

    context 'generate random shortcode when param shortcode is empty' do
      let(:url) { 'http://example.com' }
      let(:short_code) { '' }

      it 'succeeds' do
        expect(context).to be_success
        expect(context.result).to match(/^[0-9a-zA-Z_]{6}$/)
      end
    end

    context 'when url not present' do
      let(:url) { '' }
      let(:short_code) { 'example' }

      it 'fails' do
        expect(context).to be_failure
      end
    end

    context 'when shortcode is already in use' do
      let(:url) { 'http://example' }
      let(:short_code) { 'example' }

      before do
        ShortCode.create(url, short_code)
      end
      it 'fails' do
        expect(context).to be_failure
      end
    end

    context 'when shortcode is not valid' do
      let(:url) { 'http://example' }
      let(:short_code) { 'e1' }

      it 'fails' do
        expect(context).to be_failure
      end
    end
  end
end
