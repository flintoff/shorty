FROM ruby:2.5.1

RUN mkdir /usr/app
WORKDIR /usr/app

COPY . /usr/app/
RUN bundle install

# expose port
EXPOSE 9292
