# frozen_string_literal: true

require 'grape'
require 'json'
require 'pry'

require_relative './../lib/short_code'
require_relative './../commands/url_create'
require_relative './../commands/url_get'
require_relative './../commands/url_stats'

require_relative './v1'

# API
class API < Grape::API
  mount Api::V1::Root
end
