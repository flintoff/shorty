# frozen_string_literal: true

# The API v1
module Api
  module V1
    class Root < Grape::API
      format :json
      error_formatter :json, Grape::ErrorFormatter::Txt
      version 'v1', using: :header, vendor: 'api', format: :json

      post '/shorten' do
        command = UrlCreate.call(params[:url], params[:shortcode])
        if command.success?
          { shortcode: command.result }
        else
          error = command.errors.values.flatten.first
          error!(error[:message], error[:code])
        end
      end

      get '/:shortcode' do
        command = UrlGet.call(params[:shortcode])
        if command.success?
          status 302
          "Location: #{command.result}"
        else
          error = command.errors.values.flatten.first
          error!(error[:message], error[:code])
        end
      end

      get '/:shortcode/stats' do
        command = UrlStats.call(params[:shortcode])
        if command.success?
          command.result
        else
          error = command.errors.values.flatten.first
          error!(error[:message], error[:code])
        end
      end
    end
  end
end
