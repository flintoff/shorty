# frozen_string_literal: true

# Shared methods for storing short codes
module ShortCode
  module_function

  def create(url, short_code)
    File.write("#{dir}/#{short_code}.json", {
      url: url,
      startDate: Time.now.utc.to_f
    }.to_json)
  end

  def delete(short_code)
    File.delete("#{dir}/#{short_code}.json")
    get_redirect_list(short_code).each do |f|
      File.delete(f)
    end
  end

  def dir
    File.absolute_path(File.join(Dir.pwd, 'storage'))
  end

  def exist?(short_code)
    Dir["#{dir}/#{short_code}.json"].size == 1
  end

  def valid?(short_code)
    short_code =~ /^[0-9a-zA-Z_]{4,}$/
  end

  def generate
    [*('a'..'z'), *('0'..'9')].shuffle[6, 6].join
  end

  def iso8601(timestamp)
    Time.at(timestamp.to_f).utc.strftime('%Y-%m-%dT%H:%M:%S.%LZ')
  end

  def get_content(short_code)
    JSON.parse(File.read("#{dir}/#{short_code}.json"))
  end

  def get_redirect_count(short_code)
    get_redirect_list(short_code).size
  end

  def get_redirect_list(short_code)
    Dir["#{dir}/#{short_code}_*.txt"]
  end

  def get_last_seen_date(short_code)
    max = 0
    get_redirect_list(short_code).each do |f|
      t = f.scan(/_(\d+\.\d+)\.txt/).to_a.flatten.first.to_f
      max = t if t > max
    end
    max
  end

  def increment_stats(short_code)
    File.write("#{dir}/#{short_code}_#{Time.now.utc.to_f}.txt", '')
  end

  def flush_all
    [Dir["#{dir}/*.txt"], Dir["#{dir}/*.json"]].flatten.each do |f|
      File.delete(f)
    end
  end
end
